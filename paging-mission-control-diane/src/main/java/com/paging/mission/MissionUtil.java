package com.paging.mission;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MissionUtil {

	public MissionUtil() {
		// TODO Auto-generated constructor stub
	}
	
	public static Date stringToDate(String dateStr) {
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
		try {
			return dateFormat.parse(dateStr);
		} catch (ParseException e) {
			System.out.println("invalid date format");
		}
		return null;
	}
	
	public static String dateToString(Date indate)
	{
	   String dateString = null;
	   SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
	   try {
		   dateString = dateFormat.format( indate );
	   } catch (Exception ex ){
		   System.out.println(ex);
	   }
	   return dateString;
	}
	
	public static String dateToUTCFormat(Date aDate) {
		String dateString = null;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		try {
			dateString = dateFormat.format(aDate);
		} catch (Exception ex ){
			System.out.println(ex);
		}
		return dateString;
	}
		
	public static Date add5MinutesToDate(Date beforeTime){
		//Date newTime = new Date(beforeTime.getTime());
		Calendar cal = Calendar.getInstance();
		cal.setTime(beforeTime);
		cal.add(Calendar.MINUTE, 5);
		return cal.getTime();
	}

}
