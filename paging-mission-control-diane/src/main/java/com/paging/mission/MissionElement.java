package com.paging.mission;

import java.math.BigDecimal;
import java.util.Date;

public class MissionElement implements Comparable<MissionElement>{

	public MissionElement() {
	}
	
	private Date timestamp;
	private Long satelliteId;
	private BigDecimal redHighLimit;
	private Long yellowHighLimit;
	private Long yellowLowLimit;
	private BigDecimal redLowLimit;
	private BigDecimal rawValue;
	private String component;
	
	public MissionElement(String[] newElement) {
		this.timestamp = MissionUtil.stringToDate(newElement[0]);
		this.satelliteId = Long.valueOf(newElement[1]);
		this.redHighLimit = new BigDecimal(newElement[2]);
		this.yellowHighLimit = Long.valueOf(newElement[3]);	
		this.yellowLowLimit = Long.valueOf(newElement[4]);	
		this.redLowLimit = new BigDecimal(newElement[5]);	
		this.rawValue = new BigDecimal(newElement[6]);	
		this.component = newElement[7];
	}
	
	public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	public Long getSatelliteId() {
		return satelliteId;
	}
	public void setSatelliteId(Long satelliteId) {
		this.satelliteId = satelliteId;
	}
	public BigDecimal getRedHighLimit() {
		return redHighLimit;
	}
	public void setRedHighLimit(BigDecimal redHighLimit) {
		this.redHighLimit = redHighLimit;
	}
	public Long getYellowHighLimit() {
		return yellowHighLimit;
	}
	public void setYellowHighLimit(Long yellowHighLimit) {
		this.yellowHighLimit = yellowHighLimit;
	}
	public Long getYellowLowLimit() {
		return yellowLowLimit;
	}
	public void setYellowLowLimit(Long yellowLowLimit) {
		this.yellowLowLimit = yellowLowLimit;
	}
	public BigDecimal getRedLowLimit() {
		return redLowLimit;
	}
	public void setRedLowLimit(BigDecimal redLowLimit) {
		this.redLowLimit = redLowLimit;
	}
	public BigDecimal getRawValue() {
		return rawValue;
	}
	public void setRawValue(BigDecimal rawValue) {
		this.rawValue = rawValue;
	}
	public String getComponent() {
		return component;
	}
	public void setComponent(String component) {
		this.component = component;
	}

	@Override
	public int compareTo(MissionElement ele) {
		int fld1 = this.getSatelliteId().compareTo(((MissionElement) ele).getSatelliteId());
		if (fld1 == 0) {
			return this.getTimestamp().compareTo(((MissionElement) ele).getTimestamp());
		}
		return fld1;
	 }	
}
