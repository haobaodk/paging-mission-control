package com.paging.mission;

import java.util.Date;

public class OutputElement {

	public OutputElement() {
		// TODO Auto-generated constructor stub
	}
	
	private Long satelliteId;
	private String severity;
	private String component;
	private String timestamp;
	
	public OutputElement(Long sateliteId, String severity, String component, Date timestamp) {
		this.timestamp = MissionUtil.dateToUTCFormat(timestamp);	
		this.satelliteId = sateliteId;
		this.severity = severity;;
		this.component = component;	
	}
	
	public Long getSatelliteId() {
		return satelliteId;
	}
	public void setSatelliteId(Long satelliteId) {
		this.satelliteId = satelliteId;
	}
	public String getSeverity() {
		return severity;
	}
	public void setSeverity(String severity) {
		this.severity = severity;
	}
	public String getComponent() {
		return component;
	}
	public void setComponent(String component) {
		this.component = component;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

}
