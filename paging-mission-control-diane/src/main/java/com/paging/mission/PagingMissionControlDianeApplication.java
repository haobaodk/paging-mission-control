package com.paging.mission;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PagingMissionControlDianeApplication {

	public static void main(String[] args) {
		SpringApplication.run(PagingMissionControlDianeApplication.class, args);
	}

}
