package com.paging.mission;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class MissionService {

	//@Value ("classpath:templates/mission-input.txt");
	//private Resource inputfile;
	
	String pipeDelimiter = "\\|";
	String fileName = "src/main/resources/templates/mission-input.txt";
	String componentStat = "TSTAT";
	String componentBat  = "BATT";
	
	/************************************/
	/* Read input file into lines array */
	/************************************/
	public List<String> readInputFile() {
		List<String> lines = new ArrayList<String>();
		try {
		     String currentLine;	     
		     BufferedReader reader = new BufferedReader(new FileReader(fileName));
		     
		     while ((currentLine = reader.readLine()) != null) {
		    	 lines.add(currentLine);
		     }
		    reader.close();	
		} catch (FileNotFoundException e) {
			System.out.println("file not found");
		} catch (IOException e1) {
			System.out.println("IO exception" + e1);
		}
		
		return lines;	
	}
	
	/*****************************************/
	/* split input file using pipe delimiter */
	/* create new MissionElement object      */
	/*****************************************/
	public List<MissionElement> parseInputLines(List<String> inputLines) {
		
		List<MissionElement> elements = new ArrayList<MissionElement>();
		
		inputLines.forEach(ele->{
			String[] splitElements = ele.split(pipeDelimiter);
			elements.add(new MissionElement(splitElements));
		});
		return elements;
	}
	
	/*******************************************************/
	/* Check for Battery Voltage Alert violation condition */
	/* Check for Thermostat Alert violation condition      */
	/*******************************************************/
	public List<OutputElement> checkAlerts(List<MissionElement> inputElements) {
		
		List<OutputElement> elements = new ArrayList<OutputElement>();
		
		elements.addAll(checkAlertReading(componentStat, inputElements.stream().filter(ele->componentStat.equals(ele.getComponent())).collect(Collectors.toList()) ));
		elements.addAll(checkAlertReading(componentBat, inputElements.stream().filter(ele->componentBat.equals(ele.getComponent())).collect(Collectors.toList()) ));
		
		return elements;
	}
		
	
	/*****************************************************************************/
	/* Create a group of records within 5 minutes interval for same satellite id */
	/* Count # of violations within group                                        */
	/* If 3 or more, create an OutputElement                                     */
	/*****************************************************************************/
	private List<OutputElement> checkAlertReading(String component, List<MissionElement> inputElements) {
		List<OutputElement> elements = new ArrayList<OutputElement>();
		
		List<MissionElement> copyInputElements = new ArrayList<MissionElement>();
		copyInputElements.addAll(inputElements);
		
		Collections.sort(inputElements);
		Collections.sort(copyInputElements);
		
		for (MissionElement ele : inputElements) {
			List<MissionElement> newGroup = getListWithinTimeInterval(copyInputElements, ele.getSatelliteId(), ele.getTimestamp());
			if (countAlerts(newGroup, component) >= 3) {
				if (component.equals(componentStat)) {
					elements.add(new OutputElement(newGroup.get(0).getSatelliteId(), "RED HIGH", newGroup.get(0).getComponent(), newGroup.get(0).getTimestamp()));
				} 
				if (component.equals(componentBat)) {
					elements.add(new OutputElement(newGroup.get(0).getSatelliteId(), "RED LOW", newGroup.get(0).getComponent(), newGroup.get(0).getTimestamp()));
				}
			}
		}
		return elements;
	}
	
	
	/***************************/
	/* Count # of violations   */
	/***************************/
	private int countAlerts(List<MissionElement> alertGroup, String component) {
		
		int count = 0;
		for (MissionElement ele : alertGroup) {
			if (component.equals(componentStat) && ele.getRawValue().compareTo(ele.getRedHighLimit()) > 0) {
				count++;
			}
			if (component.equals(componentBat) && ele.getRawValue().compareTo(ele.getRedLowLimit()) < 0) {
				count++;
			}
		}
		return count;
	}
	
	
	/**************************************************************/
	/* Group records within same satellite id and 5 mins interval */
	/**************************************************************/
	private List<MissionElement> getListWithinTimeInterval(List<MissionElement> copyList, Long currentId, Date currentTS) {
		List<MissionElement> newGroup = new ArrayList<MissionElement>();
		for (MissionElement ele : copyList) {
			if (ele.getSatelliteId().equals(currentId) && !ele.getTimestamp().before(currentTS) &&
					MissionUtil.add5MinutesToDate(currentTS).after(ele.getTimestamp())) {
				newGroup.add(ele);
			}
		}
		return newGroup;
	}

}
