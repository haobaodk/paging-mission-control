package com.paging.mission;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/****************************************************************************/
/*  Run instruction:                                                        */
/*  1.  Place input file to src/main/resources/templates/mission-input.txt  */
/*  2.  Run as Spring boot application in Eclipse (Java 8)                  */
/*  3.  Use URL localhost:8080/readFile                                     */
/****************************************************************************/

@RestController
public class MissionController {
	
	@Autowired 
	MissionService missionService;

	@GetMapping(path="/readFile", produces=MediaType.APPLICATION_JSON_VALUE)
	public List<OutputElement> startMission() {
		
		List<String> inputLines = missionService.readInputFile();
		
		List<MissionElement> missionElements = missionService.parseInputLines(inputLines);
		
		List<OutputElement> outputElements = missionService.checkAlerts(missionElements);
		
		return outputElements;
	}

}
